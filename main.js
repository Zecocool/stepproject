"use strict"



// Service Section
const triangle = document.querySelectorAll(".service-img");
const serviceButtons =document.querySelectorAll(".service-button");
const serviceContetn = document.querySelectorAll(".service-content");

const triangleArr = [...triangle];
const serviceButtonsArray =[...serviceButtons];
const serviceContetnArray =[...serviceContetn];

function noneServiceContent(){
    serviceButtonsArray.forEach((buttons)=>{
        serviceContetnArray.forEach((contents)=>{
            triangle.forEach((element)=>{
            buttons.classList.remove("service-button-color");
            element.classList.add("service-notimage");
        contents.classList.add("service-notcontent");

        });
        });
    });
};

serviceButtonsArray.forEach((buttons, indexButtons) => {
    buttons.addEventListener("click", () => {
        serviceContetnArray.forEach((contents, contentsIndex)=>{
            triangleArr.forEach((triangle, elementIndex)=>{
                if(indexButtons===contentsIndex && indexButtons===elementIndex){
                    noneServiceContent();
                    buttons.classList.add("service-button-color");
                    triangle.classList.remove("service-notimage");
                    contents.classList.remove("service-notcontent");
                }
            });
        });
    });
});




// Our Amazing Work Section

const menuButtons = document.querySelectorAll(".menu-button");
const hiddenMenu = document.querySelector(".menu-hidden");
const loadButtons = document.querySelectorAll(".button-louder");

const menuButtonsArray = [...menuButtons];


const graphicDesign = [
    "./imeges/graphic design/graphic-design1.jpg",
    "./imeges/graphic design/graphic-design2.jpg",
    "./imeges/graphic design/graphic-design3.jpg",
    "./imeges/graphic design/graphic-design4.jpg",
    "./imeges/graphic design/graphic-design5.jpg",
    "./imeges/graphic design/graphic-design6.jpg",
    "./imeges/graphic design/graphic-design7.jpg",
    "./imeges/graphic design/graphic-design8.jpg",
    "./imeges/graphic design/graphic-design9.jpg",
    "./imeges/graphic design/graphic-design10.jpg",
    "./imeges/graphic design/graphic-design11.jpg",
    "./imeges/graphic design/graphic-design12.jpg"
];

const webDesign =[
    "./imeges/web design/web-design1.jpg",
    "./imeges/web design/web-design2.jpg",
    "./imeges/web design/web-design3.jpg",
    "./imeges/web design/web-design4.jpg",
    "./imeges/web design/web-design5.jpg",
    "./imeges/web design/web-design6.jpg",
    "./imeges/web design/web-design7.jpg"
];

const landingPages = [
    "./imeges/landing page/landing-page1.jpg",
    "./imeges/landing page/landing-page2.jpg",
    "./imeges/landing page/landing-page3.jpg",
    "./imeges/landing page/landing-page4.jpg",
    "./imeges/landing page/landing-page5.jpg",
    "./imeges/landing page/landing-page6.jpg",
    "./imeges/landing page/landing-page7.jpg"
];

const wordPress =[
    "./imeges/wordpress/wordpress1.jpg",
    "./imeges/wordpress/wordpress2.jpg",
    "./imeges/wordpress/wordpress3.jpg",
    "./imeges/wordpress/wordpress4.jpg",
    "./imeges/wordpress/wordpress5.jpg",
    "./imeges/wordpress/wordpress6.jpg", 
    "./imeges/wordpress/wordpress7.jpg",
    "./imeges/wordpress/wordpress8.jpg",
    "./imeges/wordpress/wordpress9.jpg",
    "./imeges/wordpress/wordpress10.jpg"
];



const imagesFerstAll = webDesign
const imagesAllLouder = graphicDesign.concat( landingPages, wordPress);

const allSectionImages = [
  imagesFerstAll,
  graphicDesign,
  webDesign,
  landingPages,
  wordPress,
];


function giveImages(index) {
  allSectionImages[index].forEach((element) => {
    const boxOfImages = document.createElement("div");
    const imgHover = document.createElement("div");
    const iconWrapper = document.createElement("div");
    const icon = document.createElement("img");
    const hoverTitle = document.createElement("a");
    const hoverText = document.createElement("a");
    const img = document.createElement("img");
    hiddenMenu.style.display = "grid";
    boxOfImages.classList.add("menu-image-box");
    imgHover.classList.add("menu-image-hover");
    iconWrapper.append(icon);
    icon.src = "./imeges/service/icon.png";
    hoverTitle.classList.add("menu-image-title");
    hoverText.classList.add("menu-image-text");
    hoverTitle.innerText = "creative design ";
    hoverText.innerText = "Web Design";
    hiddenMenu.append(boxOfImages);
    boxOfImages.append(imgHover, img);
    imgHover.append(iconWrapper, hoverTitle, hoverText);
    img.src = element;
    
  });
}

function haveImagesAll() {
  loadButtons[0].style.display = "flex";
  imagesFerstAll.forEach((element, index) => {
    const boxOfImages = document.createElement("div");
    const imgHover = document.createElement("div");
    const iconWrapper = document.createElement("div");
    const icon = document.createElement("img");
    const hoverTitle = document.createElement("a");
    const hoverText = document.createElement("a");
    const img = document.createElement("img");
    hiddenMenu.style.display = "grid";
    boxOfImages.classList.add("menu-image-box");
    imgHover.classList.add("menu-image-hover");
    img.src=element
    icon.src = "./imeges/service/icon.png";
    iconWrapper.append(icon);
    hoverTitle.classList.add("menu-image-title");
    hoverTitle.innerText = " creative design";
    hoverText.classList.add("menu-image-text");
    hoverText.innerText = "Web Design";
    imgHover.append(iconWrapper, hoverTitle, hoverText);
    if (index <= 11) {
      img.src = imagesFerstAll[index];
      boxOfImages.append(imgHover, img);
      hiddenMenu.append(boxOfImages);
    }
  });
}

haveImagesAll();



let imgLabel = 0;

loadButtons[0].addEventListener("click", (event) => {
  event.target.classList.add("load");


  setTimeout(() => {
    for (let i = 0; i < 12; i++) {
      const boxOfImages = document.createElement("div");
      const imgHover = document.createElement("div");
      const iconWrapper = document.createElement("div");
      const icon = document.createElement("img");
      const hoverTitle = document.createElement("a");
      const hoverText = document.createElement("a");
      const img = document.createElement("img");
      boxOfImages.classList.add("menu-image-box");
      imgHover.classList.add("menu-image-hover");
      icon.src = "./imeges/service/icon.png";
      iconWrapper.append(icon);
      hoverTitle.classList.add("menu-image-title");
      hoverTitle.innerText = " ";
      hoverText.classList.add("menu-image-text");
      hoverText.innerText = "Web Design";
      imgHover.append(iconWrapper, hoverTitle, hoverText);
      img.src = imagesAllLouder[imgLabel];
      boxOfImages.append(imgHover, img);
      hiddenMenu.append(boxOfImages);
      imgLabel++;
      if (imgLabel === 24) {
        imgLabel = 0;
        loadButtons[0].style.display = "none";
      }
    }
    event.target.classList.remove("load");
  }, 2000);
});


function noneMenuHidden() {
    menuButtonsArray.forEach((element) => {
      element.classList.remove("menu-button-active");
      hiddenMenu.style.display = "none";
      hiddenMenu.innerText = "";
    });
  }
  noneMenuHidden();

menuButtonsArray.forEach((menuButton, index) => {
    menuButton.addEventListener("click", () => {
      if (index === 0) {
        noneMenuHidden();
        menuButton.classList.add("menu-button-active");
        haveImagesAll();
      } else if (index === 1 || index === 2 || index === 3 || index === 4) {
        loadButtons[0].style.display = "none";
        noneMenuHidden();
        menuButton.classList.add("menu-button-active");
        giveImages(index);
      }
    });
  });

  // Breaking News Section
  const newsGrid = document.querySelector(".news-contents");


const newsImages = [
    "./imeges/breakingnews/Layer 47.png",
    "./imeges/breakingnews/Layer 50.png",
    "./imeges/breakingnews/Layer 51.png",
    "./imeges/breakingnews/Layer 52.png",
    "./imeges/breakingnews/Layer 53.png",
    "./imeges/breakingnews/Layer 54.png",
    "./imeges/breakingnews/Layer 55.png",
    "./imeges/breakingnews/Layer 56.png",
]

function breakingNews (){
    for(let i=0; i<newsImages.length; i++){
        const newsContent =document.createElement("div");
        newsContent.className = "news-content";
        const newsContentImg =document.createElement("img");
        newsContentImg.className ="news-content-img";
        const newsContentDate =document.createElement("div");
        newsContentDate.className= "news-content-date";
        const newsContentDay =document.createElement("p");
        newsContentDay.className = "news-content-day";
        const newsContentMonth = document.createElement("p");
        newsContentMonth.className= "news-content-month";
        const newsContentText = document.createElement("div");
        newsContentText.className = "news-content-text";
        const newsContentTitle = document.createElement("p");
        newsContentTitle.className= "news-content-title";
        const newsContentComents =document.createElement("div");
        newsContentComents.className="news-content-coments";
        const newsContentsComentsLast= document.createElement("a")
        newsContentsComentsLast.className ="news-coments-last";
        const newsContentComentsAll =document.createElement("a");
        newsContentComentsAll.className="news-coments-all";
        newsGrid.append(newsContent);
        newsContent.append(newsContentImg);
       newsContentImg.src =newsImages[Math.floor(Math.random()*8)];
       newsContent.append(newsContentDate);
       newsContentDate.append(newsContentDay);
       newsContentDay.innerText=Math.floor(Math.random()*30)+ +1;
       newsContentDate.append(newsContentMonth);
       newsContentMonth.innerText= "Feb";
       newsContent.append(newsContentText);
       newsContentText.append(newsContentTitle);
       newsContentTitle.innerText ="Amazing Blog Post";
       newsContentText.append(newsContentComents);
       newsContentComents.append(newsContentsComentsLast);
       newsContentsComentsLast.innerText ="By Admin";
       newsContentsComentsLast.href="#";
       newsContentsComentsLast.addEventListener("click", (e)=>{
        e.preventDefault();
       });
       newsContentComents.append(newsContentComentsAll);
       newsContentComentsAll.innerText=`${
        Math.floor(Math.random()*66)+ +1
       }coments`;
       newsContentComentsAll.addEventListener("click",(e) =>{
        e.preventDefault();
       });




    }
}

breakingNews ();



  // ! People Say section

  const people = document.querySelector(".people-wrapper");
  const peopleImageSelected = document.querySelector(".peoples-selected");
  const peoplesSelected = document.querySelector(".peoples-selected");
  const leftPointer = document.querySelector(".pointer-left");
  const rightPointer = document.querySelector(".pointer-right");
  const employName = document.querySelectorAll(".employee-name");
  const peopleStory = document.querySelectorAll(".people-story");
 
  const peopleArray = [...document.querySelectorAll(".people-wrapper")];

  function deletedSelected() {
    peopleArray.forEach((e) => {
      e.classList.remove("selected");
    });
  }
  
  function removeStart() {
    employName.forEach((e) => {
      e.parentElement.classList.remove("start");
    });
  }
  
  function getName() {
    employName.forEach((e) => {
      if (
        e.innerText.toLowerCase() ===
        peoplesSelected.firstChild.dataset.say.toLowerCase()
      ) {
        removeStart();
        e.parentElement.classList.add("start");
      }
    });
  }
  
  function getStory() {
    peopleStory.forEach((e) => {
      if (
        e.dataset.say.toLowerCase() ===
        peoplesSelected.firstChild.dataset.say.toLowerCase()
      ) {
        peopleStory.forEach((e) => {
          e.classList.remove("start");
        });
        e.classList.add("start");
      }
    });
  }


  let reapetElement = peopleArray[0].cloneNode(true);
  reapetElement.style.position = "static";
  peopleArray[0].classList.add("selected");
  peoplesSelected.append(reapetElement);
  reapetElement.classList.add("selected-scin");
  getName();
  getStory();

  let selectedIndex = 0;

for (let i = 0; i < peopleArray.length; i++) {
    peopleArray[i].addEventListener("click", () => {
      deletedSelected();
      peopleImageSelected.innerHTML = "";
      let reapetElement = peopleArray[i].cloneNode(true);
      reapetElement.style.position = "static";
      peopleArray[i].classList.add("selected");
      peopleImageSelected.append(reapetElement);
      reapetElement.classList.add("selected-scin");
      getName();
      getStory();
  
      selectedIndex = i;
    });
  }
  
  leftPointer.addEventListener("click", () => {
    deletedSelected();
    peopleImageSelected.innerHTML = "";
    selectedIndex--;
    if (selectedIndex < 0) selectedIndex = peopleArray.length - 1;
    let cloneElement = peopleArray[selectedIndex].cloneNode(true);
    cloneElement.style.position = "static";
    peopleArray[selectedIndex].classList.add("selected");
    peopleImageSelected.append(cloneElement);
    cloneElement.classList.add("selected-scin");
    getName();
    getStory();
  });
  
  rightPointer.addEventListener("click", () => {
    deletedSelected();
    peopleImageSelected.innerHTML = "";
    selectedIndex++;
    if (selectedIndex === peopleArray.length) selectedIndex = 0;
    let cloneElement = peopleArray[selectedIndex].cloneNode(true);
    cloneElement.style.position = "static";
    peopleArray[selectedIndex].classList.add("selected");
    peopleImageSelected.append(cloneElement);
    cloneElement.classList.add("selected-scin");
    getName();
    getStory();
  });



  
//   Gallery of best images


const gridContent = document.querySelector(".grid");
let msnry;
window.onload = function () {
  msnry = new Masonry(gridContent, {
    columnWidth: 373,
    itemSelector: ".gallery-images",
    gutter: 20,
  });
};

const imagesStore = [
  "https://dr.savee-cdn.com/things/thumbnails/5/e/c89d6c12e5b52a6adbebbf.webp",
"https://dr.savee-cdn.com/things/thumbnails/6/0/9b53b900e645789cfc433b.webp",
  "https://dr.savee-cdn.com/things/thumbnails/5/f/245cb58f50b17acb22733d.webp",
"https://dr.savee-cdn.com/things/thumbnails/6/1/ed4f14aba2a453c7eefe97.webp",
  "https://dr.savee-cdn.com/things/5/d/77763214bfda5acde32000.gif",
"https://dr.savee-cdn.com/things/thumbnails/6/2/ad7dc7c683b2178659d5b0.webp",
  "https://dr.savee-cdn.com/things/thumbnails/5/e/c89d622927c02a7f551f47.webp",
"https://dr.savee-cdn.com/things/thumbnails/6/2/836d4e902a78e718558c4b.webp",
  "https://dr.savee-cdn.com/things/thumbnails/5/b/c3b8b9fb5a166a1c0ff5ec.webp",
"https://dr.savee-cdn.com/things/thumbnails/6/2/51d5579f4a147eed1db397.webp",
  "https://dr.savee-cdn.com/things/thumbnails/5/c/0c06f187d6044201f95b34.webp",
  "https://dr.savee-cdn.com/things/thumbnails/5/8/d01d46811b1b1526a819d7.webp",
  "https://dr.savee-cdn.com/things/thumbnails/5/f/e99e38cf66f578897d215d.webp",
  "https://dr.savee-cdn.com/things/thumbnails/5/e/c89d6ba63eb12a8e646285.webp",
  "https://dr.savee-cdn.com/things/thumbnails/5/e/c89d67a63eb12a8e646282.webp",
  "https://dr.savee-cdn.com/things/5/9/8848c00c12d97c4dfeae7b.webp",
  "https://dr.savee-cdn.com/things/thumbnails/6/3/8adcf40a0f8925c1ba6114.webp",
  "https://dr.savee-cdn.com/things/thumbnails/5/f/c6b4cdf1ba9e056257b78d.webp",
  "https://dr.savee-cdn.com/things/thumbnails/5/9/7b11cd6b2925748380df1f.webp",
  "https://dr.savee-cdn.com/things/thumbnails/5/f/c6b4cdf1ba9e056257b78d.webp",
  "https://dr.savee-cdn.com/things/5/9/71dd6a718d6b1b590822fb.webp",
  "https://dr.savee-cdn.com/things/thumbnails/5/9/80e68c6d83c26f4e989aa5.webp",
  "https://dr.savee-cdn.com/things/5/9/711cecace78b5e43fb5a43.webp",
  "https://dr.savee-cdn.com/things/thumbnails/5/9/760d4b8c3f3873066e8618.webp",
  "https://dr.savee-cdn.com/things/6/1/7324066d536b9dc6e3baf6.gif",
];



function showAlImages() {
  imagesStore.forEach((element, index) => {
    const images = document.createElement("img");
    const imgesBox = document.createElement("div");
    if (index <= 10) {
      images.src = imagesStore[index];
      imgesBox.classList.add("gallery-images");
      imgesBox.append(images);
      gridContent.append(imgesBox);
    }
  });
}
showAlImages();




imagesStore.forEach((elemetImages) => {
  loadButtons[1].addEventListener("click", () => {
    loadButtons[1].classList.add("load");
    setTimeout(() => {
      const elemntss = [];
      const contentBox = document.createElement("div");
      const img = document.createElement("img");
      const imgesBox = document.createElement("div");
      img.src = elemetImages;
      imgesBox.classList.add("gallery-images");
      imgesBox.append(img);
      contentBox.append(imgesBox);
      elemntss.push(imgesBox);
      loadButtons[1].classList.remove("load");
      gridContent.append(contentBox);
      msnry.appended(elemntss);
    }, 2000);
  });
});
